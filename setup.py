#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-ldap

# Copyright 2010-2016 Mir Calculate. http://www.calculate-linux.org
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
from os import path
from glob import glob
import sys

import distutils
from distutils.core import setup
from distutils.command.install_data import install_data
import itertools

icon_system_path = ['/usr/share/icons/Calculate/16x16/client-gui']
icon_path = 'data/images/'
icon_names = ['console_ok.png', 'console_cancel.png']
data_files = list(itertools.product(icon_system_path, [[icon_path+x] for x in icon_names]))

data_files.append(('/usr/share/pixmaps',['data/calculate-console-online.svg',
                                         'data/calculate-console-offline.svg',
                                         'data/calculate-console-update.svg',
                                         'data/calculate-console-update-no.svg']))
data_files.append(('/usr/share/applications',
                           ['data/cl-console-gui-install.desktop',
                            'data/cl-console-gui-update.desktop',
                            'data/cl-console-gui-update-settings.desktop',
                            'data/cl-console-gui.desktop',
                            'data/cl-console-gui-settings.desktop']))
data_files.append(('/etc/xdg/autostart',
                           ['data/cl-update-checker.desktop']))
data_files.append(('/usr/share/calculate/themes/install',
                   ['data/install/conf','data/install/welcome.jpg',
                    'data/install/finish.jpg']))
for theme in ("gnome","hicolor"):
    for size in [16,22,32,24,48,64,72,96,128]:
        for iconname in ("cl-console-gui","console-gui-install",
                         "cl-console-gui-install",
                         "console-gui-update",):
            data_files.append(('/usr/share/icons/%s/%dx%d/apps' %(theme,size,size),
                           glob('data/%dx%d/%s.png' %(size,size,iconname))))
for theme in ("gnome","hicolor"):
    for iconname in ("toolbar-disconnect",
        "toolbar-session",
        "toolbar-connect",
        "toolbar-certificates",
        "toolbar-all-settings",
        "toolbar-help",
        "toolbar-tools",
        "toolbar-processes",
        "console","install", "update",
        "builder-break",
        "builder-image",
        "builder-prepare",
        "builder-profile",
        "builder-restore",
        "builder-update",
        "client-passwd",
        "client",
        "core-custom",
        "core-dispatch",
        "core-group-show",
        "core-request-show",
        "core-restart",
        "core-setup",
        "core-variables",
        "core-view-cert",
        "desktop-logout",
        "desktop",
        "install-flash",
        "install-pxe",
        "setup-audio",
        "setup-boot",
        "setup-locale",
        "setup-network",
        "setup-session",
        "setup-system",
        "setup-update",
        "setup-video",
        "update-livemenu",
        "update-profile",
        "unix-usershow",
        "unix-groupshow",
        "ldap-setup",
        "unix-setup",
        "backup",
        "backup-restore",
        ):
        data_files.append(('/usr/share/icons/%s/scalable/apps' %theme,
                       glob('data/scalable/calculate-%s.svg' %iconname)))

__version__ = "3.5.0_alpha1"
__app__ = "calculate-console-gui"
module_name = "calculate.consolegui"
packages = [
    "calculate."+str('.'.join(root.split(os.sep)[1:]))
    for root, dirs, files in os.walk('pym/consolegui')
    if '__init__.py' in files
]

setup(
    name = __app__,
    version = __version__,
    description = "GUI client for WSDL server",
    author = "Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    data_files = data_files,
    package_dir = {'calculate.consolegui': "pym/consolegui"},
    packages = packages,
    scripts = (glob('bin/*')),
    cmdclass={'install_data': install_data}
)
