#-*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from .. import qt

class StatusLabel(qt.QLabel):
    def __init__(self, name, parent):
        super().__init__(name, parent)
        self.setWordWrap(True)
        self.setAttribute(qt.Qt.WA_DeleteOnClose)

class StatusFieldWgt(qt.QWidget):
    
    def __init__(self, parent, layout):
        super().__init__(parent)
        self.setLayout(layout)
        self.setAttribute(qt.Qt.WA_DeleteOnClose)


class StatusField (qt.QScrollArea):
    # bottom in main frame
    def __init__(self, parent, ClientObj):
        super().__init__(parent)
        self.layout = qt.QVBoxLayout()
        self.status_widget = StatusFieldWgt(self, self.layout)
        self.setWidget(self.status_widget)
        
        # for clear memory after closed this window
        self.setAttribute(qt.Qt.WA_DeleteOnClose)

    def addMessage(self, text):
        self.layout.addWidget(StatusLabel(_(text), self))
        if self.layout.count() > 30:
            self.layout.removeItem(self.layout.itemAt(0))
        self.status_widget = StatusFieldWgt(self, self.layout)
        self.setWidget(self.status_widget)
        self.verticalScrollBar().setValue(self.verticalScrollBar().maximum())
        
        # update size children widget
        self.resizeEvent(0)

    def resizeEvent(self, resize_var):
        width = self.size().width()
        if width > 30:
            if self.verticalScrollBar().isVisible():
                self.status_widget.setFixedWidth(width - \
                                    self.verticalScrollBar().size().width() - 5)
            else:
                self.status_widget.setFixedWidth(width - 6)
        # update size children widget
        self.status_widget.updateGeometry()
