#-*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


from .. import qt
from .more import TopMenu, HelpMenu#, FlowLayout

class MainMenu(qt.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
#        self.hlayout = FlowLayout(self)
        self.hlayout = qt.QHBoxLayout(self)
        self.hlayout.setAlignment(qt.Qt.AlignLeft)

######### Back on one step
        self.Back = TopMenu(_('All Settings'),
            ['calculate-toolbar-all-settings', 'go-previous'], self)
        self.Back.setStatusTip(_('All settings'))
        self.Back.setToolTip(_('All settings'))
        self.Back.clicked.connect(parent.back)
        self.Back.setVisible(False)
        self.hlayout.addWidget(self.Back)

########## Display all methods
#        self.Methods = TopMenu(_('Methods'),['go-home', 'gohome'], self)
#        self.Methods.setStatusTip(_('Display Methods'))
#        self.Methods.setToolTip(_('Display Methods'))
#        self.Methods.clicked.connect(parent.display_methods)
#        self.Methods.setVisible(False)
#
#        self.hlayout.addWidget(self.Methods)

######### View information about the running processes
        self.Processes = TopMenu(_('Processes'),['calculate-toolbar-processes',
                                                 'applications-system'], self)
        self.Processes.setStatusTip(_('View information about running '
                                    'processes'))
        self.Processes.setToolTip(_('View information about running '
                                    'processes'))
        self.Processes.clicked.connect(parent.view_processes)
        self.Processes.setVisible(False)
        self.hlayout.addWidget(self.Processes)

######### View information about current session
        self.Session = TopMenu(_('Session'), ['calculate-toolbar-session', 
                                              'document-edit-verify',
                                              'document-revert','edit-find'],
                                              self)
        self.Session.setStatusTip \
                            (_('View information about the current session'))
        self.Session.setToolTip \
                            (_('View information about the current session'))
        self.Session.clicked.connect(parent.view_session_info)
        self.Session.setVisible(False)
        self.hlayout.addWidget(self.Session)

######### Exit this session
        self.Disconnect = TopMenu(_('Disconnect'),
                            ['calculate-toolbar-disconnect',
                             'network-disconnect', 'connect_no'], self)
        self.Disconnect.setStatusTip(_('Disconnect'))
        self.Disconnect.setToolTip(_('Disconnect from the current server'))
        self.Disconnect.clicked.connect(parent.disconnect)
        self.Disconnect.setVisible(False)
        self.hlayout.addWidget(self.Disconnect)

######### Connection
        self.Connect = TopMenu(_('Connect'), \
                            ['calculate-toolbar-connect',
                             'network-connect','connect_established'], self)
        self.Connect.setStatusTip(_('Connect'))
        self.Connect.setToolTip(_('Connect'))
        self.Connect.clicked.connect(parent.connecting)
        self.hlayout.addWidget(self.Connect)

######### Window work with certificates
        self.Certificates = TopMenu(_('Certificates'), \
                        ['calculate-toolbar-certificates',
                         'view-certificate', 'application-certificate'], self)
        self.Certificates.setStatusTip(_('Certificates manager'))
        self.Certificates.setToolTip(_('Certificates manager'))
        self.Certificates.clicked.connect(parent.work_with_certificates)
        self.hlayout.addWidget(self.Certificates)

######### Tools dialog
        self.Tool = TopMenu(_('Tools'),['calculate-toolbar-tools',
                                        'preferences-other',
                                        'preferences-system'], self)
        self.Tool.setStatusTip(_('Application settings'))
        self.Tool.setToolTip(_('Application settings'))
        self.Tool.clicked.connect(parent.tools)
        self.hlayout.addWidget(self.Tool)

######### View help information
        self.Help = HelpMenu(_('Help'),
            ['calculate-toolbar-help', 'help-about','help-browser'],
            parent, parent.ClientObj._parent)
        self.Help.setStatusTip(_('Help'))
        self.hlayout.addWidget(self.Help)

######### Exit Program
#        self.Exit=TopMenu(_('Close'),['application-exit', 'system-log-out'],\
#                                                                        self)
#        self.Exit.setStatusTip(_('Close Tab'))
#        self.Exit.setToolTip(_('Close Tab'))
#        self.Exit.clicked.connect(parent.ClientObj._parent.close_tab)
#        
#        self.hlayout.addWidget(self.Exit)

        self.hlayout.setContentsMargins(2,2,2,2)
        self.hlayout.setSpacing(0)

        self.setFixedHeight(self.hlayout.sizeHint().height())
        self.setAttribute(qt.Qt.WA_DeleteOnClose)

    def refresh(self):
######### Back on one step
        self.Back.setText(_('All Settings'))
        self.Back.setStatusTip(_('All settings'))
        self.Back.setToolTip(_('All settings'))

######### View information about the running processes
        self.Processes.setText(_('Processes'))
        self.Processes.setStatusTip(_('View information about running '
                                    'processes'))
        self.Processes.setToolTip(_('View information about running '
                                    'processes'))

######### View information about current session
        self.Session.setText(_('Session'))
        self.Session.setStatusTip \
                            (_('View information about the current session'))
        self.Session.setToolTip \
                            (_('View information about the current session'))

######### Exit this session
        self.Disconnect.setText(_('Disconnect'))
        self.Disconnect.setStatusTip(_('Disconnect'))
        self.Disconnect.setToolTip(_('Disconnect from the current server'))

######### Connection
        self.Connect.setText(_('Connect'))
        self.Connect.setStatusTip(_('Connect'))
        self.Connect.setToolTip(_('Connect'))

######### Window work with certificates
        self.Certificates.setText(_('Certificates'))
        self.Certificates.setStatusTip(_('Certificates manager'))
        self.Certificates.setToolTip(_('Certificates manager'))

######### Tools dialog
        self.Tool.setText(_('Preferences'))
        self.Tool.setStatusTip(_('Application preferences'))
        self.Tool.setToolTip(_('Application preferences'))

######### View help information
        self.Help.setText(_('Help'))
        self.Help.setStatusTip(_('Help'))
#        self.Help.setToolTip(_('About Application'))

######### Exit Program
#        self.Exit.setText(_('Close'))
#        self.Exit.setStatusTip(_('Close Tab'))
#        self.Exit.setToolTip(_('Close Tab'))
