#-*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from .. import qt
from .more import get_system_rgb, ParameterWindow


class Delegate(qt.QStyledItemDelegate):
    def __init__(self, parent = None):
        super().__init__(parent)

    def setView(self, view):
        self.view = view

    def paint(self, painter, option, index):
        o = qt.QStyleOptionViewItemV4(option)
        self.initStyleOption(o, index)
        if o.state & qt.QStyle.State_MouseOver:
            self.view.setMouseOver(index.row())

        o.state &= ~qt.QStyle.State_MouseOver
        qt.QStyledItemDelegate.paint(self, painter, o, index)

class SelectedTableWidget(qt.QTableWidget):
    def __init__(self, rows, columns, parent, readonly = False, brief = False):
        super().__init__(rows, columns, parent)
        # print("CREATED selected table widget")
        self.currHovered = -1
        self.brief = brief
        self.bFixedHeight = True

        delegate = Delegate()
        delegate.setView(self)
        self.setItemDelegate(delegate)
        self.setMouseTracking(True)

        self.readonly = readonly
        self.setSelectionMode(qt.QAbstractItemView.NoSelection)
        self.horizontalHeader().moveSection(-1,0)
        self.horizontalHeader().setFixedHeight(24)
        self.horizontalHeader().setMinimumSectionSize(48)
        self.setFrameStyle(qt.QFrame.NoFrame)
        self.verticalHeader().hide()

        item_background = 'transparent' if brief else get_system_rgb(
            self, ParameterWindow)
        #'rgba(255,255,255,255)'
        self.background_color = 'transparent' if brief else get_system_rgb(
            self, ParameterWindow)
            #'#FFFFFF'

        self.horizontalHeader().setStyleSheet(
            "QHeaderView {background-color: transparent;"
            "border: none; }"
            'QHeaderView::section {background-color: %s;'
            % item_background +
            'border: 1px solid #B3ABA7; border-radius: 1px;}')
        # self.horizontalHeader().setStyleSheet(
        #     "QHeaderView {background-color: rgb(255,0,0);"
        #     "border: none; }"
        #     'QHeaderView::section {background-color: rbg(0,0,255);'
        #     'border: 1px solid #B3ABA7; border-radius: 1px;}')
        self.unsetErrorBorder()

    def setColor(self, row = -1):
        if self.brief:
            return
        if row == -1:
            for row in range (0, self.rowCount()):
                for col in range(0, self.columnCount()):
                    item = self.item(row, col)
                    if not item:
                        continue
                    brush = qt.QBrush(
                        self.palette().color(ParameterWindow))
                    item.setBackground(brush)

    def mousePressEvent(self, event):
        if self.readonly:
            event.ignore()
            return
        if event.button() != qt.Qt.LeftButton:
            event.ignore()
        else:
            qt.QTableWidget.mousePressEvent(self, event)
            event.accept()

    def setMouseOver(self, row):
        if row == self.currHovered or self.readonly:
            return
        for col in range (0, self.columnCount()):
            item = self.item(row, col)
            if not item:
                continue
            bg = self.palette().color(qt.QPalette.Highlight)
            item.setBackground(qt.QBrush(bg))
                #qt.QColor(220,220,220,255)))

        if self.currHovered != -1:
            self.disableMouseOver()
        self.currHovered = row
#        self.setCursor(qt.Qt.PointingHandCursor)

    def disableMouseOver(self):
        if self.readonly:
            return
#        self.setCursor(qt.Qt.ArrowCursor)
        for col in range (0, self.columnCount()):
            item = self.item(self.currHovered, col)
            if not item:
                continue
            bg = self.palette().color(ParameterWindow)
            item.setBackground(qt.QBrush(bg))
            #item.setBackground(qt.QBrush(qt.QColor("white")))

    def mouseMoveEvent(self, event):
        qt.QTableView.mouseMoveEvent(self, event)

    def leaveEvent(self, event):
        if self.readonly:
            return
        self.disableMouseOver()
        self.currHovered = -1

    def unsetErrorBorder(self):
        self.setStyleSheet("QTableWidget {"
                         "border: none;"
                         "gridline-color: #B3ABA7;"
                         "background-color: %s;}" %
                         self.background_color)

    def setErrorBorder(self):
        self.setStyleSheet("QTableWidget {"
                         "border: 1px solid red;"
                         "background-color: %s;}" %
                         self.background_color)
