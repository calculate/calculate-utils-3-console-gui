#-*- coding: utf-8 -*-

# Copyright 2018 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

from PyQt5 import QtGui, QtCore, QtWidgets

QAbstractItemView = QtWidgets.QAbstractItemView
QAction = QtWidgets.QAction
QApplication = QtWidgets.QApplication
QBrush = QtGui.QBrush
QButtonGroup = QtWidgets.QButtonGroup
QCheckBox = QtWidgets.QCheckBox
QColor = QtGui.QColor
QComboBox = QtWidgets.QComboBox
QCompleter = QtWidgets.QCompleter
QCursor = QtGui.QCursor
QDesktopServices = QtGui.QDesktopServices
QDialog = QtWidgets.QDialog
QFileDialog = QtWidgets.QFileDialog
QFontMetrics = QtGui.QFontMetrics
QFont = QtGui.QFont
QFrame = QtWidgets.QFrame
QGridLayout = QtWidgets.QGridLayout
QGroupBox = QtWidgets.QGroupBox
QHBoxLayout = QtWidgets.QHBoxLayout
QHeaderView = QtWidgets.QHeaderView
QIcon = QtGui.QIcon
QImageReader = QtGui.QImageReader
QIntValidator = QtGui.QIntValidator
QKeySequence = QtGui.QKeySequence
QLabel = QtWidgets.QLabel
QLayout = QtWidgets.QLayout
QLineEdit = QtWidgets.QLineEdit
QListWidget = QtWidgets.QListWidget
QMainWindow = QtWidgets.QMainWindow
QMenu = QtWidgets.QMenu
QMessageBox = QtWidgets.QMessageBox
QPainter = QtGui.QPainter
QPalette = QtGui.QPalette
QPixmap = QtGui.QPixmap
QProgressBar = QtWidgets.QProgressBar
QPushButton = QtWidgets.QPushButton
QRadioButton = QtWidgets.QRadioButton
QRegExpValidator = QtGui.QRegExpValidator
QScrollArea = QtWidgets.QScrollArea
QSizePolicy = QtWidgets.QSizePolicy
QSpacerItem = QtWidgets.QSpacerItem
QStyledItemDelegate = QtWidgets.QStyledItemDelegate
QStyleOptionViewItemV4 = QtWidgets.QStyleOptionViewItem
QStyle = QtWidgets.QStyle
QSystemTrayIcon = QtWidgets.QSystemTrayIcon
QTabBar = QtWidgets.QTabBar
QTableView = QtWidgets.QTableView
QTableWidgetItem = QtWidgets.QTableWidgetItem
QTableWidget = QtWidgets.QTableWidget
QTabWidget = QtWidgets.QTabWidget
QTextEdit = QtWidgets.QTextEdit
QToolButton = QtWidgets.QToolButton
QTransform = QtGui.QTransform
QVBoxLayout = QtWidgets.QVBoxLayout
QWidget = QtWidgets.QWidget

QObject = QtCore.QObject
QPoint = QtCore.QPoint
QRect = QtCore.QRect
QRegExp = QtCore.QRegExp
QSize = QtCore.QSize
Qt = QtCore.Qt
QThread = QtCore.QThread
QTimer = QtCore.QTimer
QUrl = QtCore.QUrl
Signal = QtCore.pyqtSignal

# use that if you need to suppress QT warnings/errors

# def handler(msg_type, msg_log_context, msg_string):
#     pass
# QtCore.qInstallMessageHandler(handler)